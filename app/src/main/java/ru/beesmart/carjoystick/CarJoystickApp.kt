package ru.beesmart.carjoystick

import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import ru.beesmart.carjoystick.dagger.DaggerAppComponent
import javax.inject.Inject

class CarJoystickApp : Application(), HasAndroidInjector {

    @Inject
    internal lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        val component = DaggerAppComponent
            .builder()
            .context(this)
            .build()
        component.inject(this)
    }

    override fun androidInjector() = dispatchingAndroidInjector

}