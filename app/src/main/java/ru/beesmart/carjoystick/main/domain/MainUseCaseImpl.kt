package ru.beesmart.carjoystick.main.domain

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import ru.beesmart.carjoystick.settings.domain.SettingsRepository
import javax.inject.Inject

class MainUseCaseImpl @Inject constructor(
    private val bluetoothProvider: BluetoothProvider,
    private val settingsRepository: SettingsRepository
): MainUseCase {
    override fun isAdapterEnabled(): Single<Boolean> {
        return bluetoothProvider.isAdapterEnabled()
    }

    override fun connectBt(): Completable {
        return settingsRepository.getSettings()
            .flatMapCompletable {settings ->
                bluetoothProvider.connectBt(settings.btAddress!!)
            }
    }
}