package ru.beesmart.carjoystick.main.android

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import ru.beesmart.carjoystick.R
import ru.beesmart.carjoystick.joystick.android.JoystickFragment
import ru.beesmart.carjoystick.main.presentation.MainContract
import ru.beesmart.carjoystick.settings.android.SettingsFragment
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainContract.View, OnNeedConnectionListener {

    @Inject lateinit var mainPresenter : MainContract.Presenter
    @Inject lateinit var navigatorHolder: NavigatorHolder

    private val navigator = SupportAppNavigator(this, R.id.fragment_container)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        mainPresenter.onCreate()
    }

    override fun onResume() {
        mainPresenter.bindView(this)
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        mainPresenter.unbindView()
        navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun showBluetoothRequest() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                mainPresenter.onCreate()
            } else {
                Toast.makeText(this, "BlueTooth не включён", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    override fun showJoystickFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, JoystickFragment())
            .commit()
    }

    override fun showSettingsFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, SettingsFragment())
            .commit()
    }

    override fun showStatus(status: String) {
        Toast.makeText(this, status, Toast.LENGTH_SHORT).show()
    }

    override fun onNeedConnect() {
        mainPresenter.connectBt()
    }

    companion object {
        private const val REQUEST_ENABLE_BT = 1
    }
}

interface OnNeedConnectionListener {
    fun onNeedConnect()
}