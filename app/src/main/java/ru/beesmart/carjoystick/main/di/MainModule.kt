package ru.beesmart.carjoystick.main.di

import dagger.Binds
import dagger.Module
import ru.beesmart.carjoystick.main.domain.MainUseCase
import ru.beesmart.carjoystick.main.domain.MainUseCaseImpl
import ru.beesmart.carjoystick.main.presentation.MainContract
import ru.beesmart.carjoystick.main.presentation.MainPresenter

@Module
abstract class MainModule {

    @Binds
    internal abstract fun bindMainUseCase(impl: MainUseCaseImpl): MainUseCase

    @Binds
    internal abstract fun bindMainPresenter(impl: MainPresenter) : MainContract.Presenter
}