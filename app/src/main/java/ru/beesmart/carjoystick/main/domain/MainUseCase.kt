package ru.beesmart.carjoystick.main.domain

import io.reactivex.Completable
import io.reactivex.Single

interface MainUseCase {
    fun isAdapterEnabled(): Single<Boolean>
    fun connectBt(): Completable
}