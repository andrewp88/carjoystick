package ru.beesmart.carjoystick.main.presentation

import io.reactivex.android.schedulers.AndroidSchedulers
import ru.beesmart.carjoystick.base.presentation.BasePresenter
import ru.beesmart.carjoystick.main.domain.MainUseCase
import ru.beesmart.carjoystick.navigation.presentation.FragmentRouter
import javax.inject.Inject

class MainPresenter @Inject constructor(
    private val mainUseCase: MainUseCase,
    private val fragmentRouter: FragmentRouter
) : BasePresenter<MainContract.View>(), MainContract.Presenter {

    private var countTryConnect = 0

    override fun onCreate() {
        mainUseCase.isAdapterEnabled()
            .observeOn(AndroidSchedulers.mainThread())
            .wrapWithProgress()
            .subscribe(
                { isEnabled ->
                    if (isEnabled) {
                        connectBt()
                    } else {
                        view?.showBluetoothRequest()
                    }

                },
                { throwable ->
//                    onError(
//                        throwable,
//                        stringResourceProvider.getStringResouce(R.string.objects_getting_error_message)
//                    )
                }
            )
            .addToComposite()
    }

    override fun connectBt() {
        mainUseCase.connectBt()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.apply {
                        showStatus("Connected")
                        fragmentRouter.replaceToJoystickScreen()
                    }
                },
                { throwable ->
                    countTryConnect++
                    view?.apply {
                        if (throwable is NullPointerException || countTryConnect > 4) {
                            fragmentRouter.navigateToSettingsScreen()
                            showStatus(throwable.message?: "not connected")
                            countTryConnect = 0
                        } else {
                            connectBt()
                        }
                    }
                }
            )
            .addToComposite()
    }

    override fun onResume() {

    }

}