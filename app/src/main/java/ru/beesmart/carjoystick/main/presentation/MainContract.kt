package ru.beesmart.carjoystick.main.presentation

import ru.beesmart.carjoystick.base.presentation.PresentationContract

interface MainContract {

    interface View : PresentationContract.View{
        fun showJoystickFragment()
        fun showSettingsFragment()
        fun showBluetoothRequest()
        fun showStatus(status: String)
    }

    interface Presenter : PresentationContract.Presenter<View>{
        fun onCreate()
//        fun getBtDevices()
        fun connectBt()
        fun onResume()
  //      fun getBoundedDevises(): Single<List<>>
    }
}