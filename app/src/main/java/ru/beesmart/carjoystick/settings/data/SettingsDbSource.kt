package ru.beesmart.carjoystick.settings.data

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.settings.domain.models.Settings

interface SettingsDbSource {
    fun getSettings(): Single<Settings>
    fun setAddress(address: String): Completable
}