package ru.beesmart.carjoystick.settings.db

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.extensions.mapToEntity
import ru.beesmart.carjoystick.extensions.mapToSettings
import ru.beesmart.carjoystick.room.db.JoystickRoom
import ru.beesmart.carjoystick.settings.data.SettingsDbSource
import ru.beesmart.carjoystick.settings.domain.models.Settings
import javax.inject.Inject

class SettingsDbSourceImpl @Inject constructor(
    private val joystickRoom: JoystickRoom
) : SettingsDbSource {
    override fun getSettings(): Single<Settings> {
        return joystickRoom.settings.getSettingsEntity()
            .switchIfEmpty(
                joystickRoom.settings.addSettings(
                    getDefaultSettings()
                        .mapToEntity()
                )
                    .andThen(joystickRoom.settings.getSettingsEntity())
                    .toSingle()
            )
            .map { it.mapToSettings() }
    }

    override fun setAddress(address: String): Completable {
        return joystickRoom.settings.setAddress(address)
    }

    private fun getDefaultSettings(): Settings {
        return Settings(
            FLASHLIGHT_ON_TAG,
            FLASHLIGHT_OFF_TAG,
            MAX_SEEKBAR_POSITION,
            true
        )
    }

    companion object {
        private const val FLASHLIGHT_ON_TAG = "\$g;"
        private const val FLASHLIGHT_OFF_TAG = "\$f;"
        const val MAX_SEEKBAR_POSITION = 100
    }
}