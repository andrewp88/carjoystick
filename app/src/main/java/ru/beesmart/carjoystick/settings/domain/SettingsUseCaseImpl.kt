package ru.beesmart.carjoystick.settings.domain

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import javax.inject.Inject

class SettingsUseCaseImpl @Inject constructor(
    private val bluetoothProvider: BluetoothProvider,
    private val settingsRepository: SettingsRepository
) : SettingsUseCase {
    override fun getBtDevices(): Single<Map<String, String>> {
        return bluetoothProvider.getBoundedDevices()
    }

    override fun setAddress(address: String): Completable {
        return settingsRepository.setAddress(address)
    }
}