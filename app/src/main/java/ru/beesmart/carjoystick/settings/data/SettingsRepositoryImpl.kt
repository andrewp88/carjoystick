package ru.beesmart.carjoystick.settings.data

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.beesmart.carjoystick.settings.domain.SettingsRepository
import ru.beesmart.carjoystick.settings.domain.models.Settings
import javax.inject.Inject

class SettingsRepositoryImpl @Inject constructor(
private val settingsDbSource: SettingsDbSource
): SettingsRepository {
    override fun getSettings(): Single<Settings> {
        return settingsDbSource.getSettings()
            .subscribeOn(Schedulers.io())
    }

    override fun setAddress(address: String): Completable {
        return settingsDbSource.setAddress(address)
            .subscribeOn(Schedulers.io())
    }
}