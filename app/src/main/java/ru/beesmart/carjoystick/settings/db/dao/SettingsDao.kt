package ru.beesmart.carjoystick.settings.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import ru.beesmart.carjoystick.settings.db.models.SettingsEntity

@Dao
abstract class SettingsDao {

    @Insert
    abstract fun addSettings(settings: SettingsEntity): Completable

    @Query("select * from settings where id = 1")
    abstract fun getSettingsEntity(): Maybe<SettingsEntity>

    @Query("UPDATE settings SET btAddress = :address WHERE id = 1")
    abstract fun setAddress(address: String): Completable
}