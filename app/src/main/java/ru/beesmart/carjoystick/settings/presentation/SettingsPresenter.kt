package ru.beesmart.carjoystick.settings.presentation

import io.reactivex.android.schedulers.AndroidSchedulers
import ru.beesmart.carjoystick.base.presentation.BasePresenter
import ru.beesmart.carjoystick.settings.domain.SettingsUseCase
import javax.inject.Inject

class SettingsPresenter @Inject constructor(
    private val settingsUseCase: SettingsUseCase
): BasePresenter<SettingsContract.View>(), SettingsContract.Presenter {
    override fun getBtDevices() {
        settingsUseCase.getBtDevices()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.showAddressList(it)
                },
                {

                }
            )
            .addToComposite()
    }

    override fun setBtAddress(address: String) {
        settingsUseCase.setAddress(address)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.connectBt()
                },
                {
                    view?.showStatus(it.message?: "bad")
                }
            )
            .addToComposite()
    }

}