package ru.beesmart.carjoystick.settings.di

import dagger.Binds
import dagger.Module
import ru.beesmart.carjoystick.settings.data.SettingsDbSource
import ru.beesmart.carjoystick.settings.data.SettingsRepositoryImpl
import ru.beesmart.carjoystick.settings.db.SettingsDbSourceImpl
import ru.beesmart.carjoystick.settings.domain.SettingsRepository
import ru.beesmart.carjoystick.settings.domain.SettingsUseCase
import ru.beesmart.carjoystick.settings.domain.SettingsUseCaseImpl
import javax.inject.Singleton

@Module
abstract class SettingsModule {

    @Singleton
    @Binds
    internal abstract fun bindSettingsDbSource(impl: SettingsDbSourceImpl): SettingsDbSource

    @Singleton
    @Binds
    internal abstract fun bindSettingsRepository(impl: SettingsRepositoryImpl): SettingsRepository

    @Singleton
    @Binds
    internal abstract fun bindSettingsUseCase(impl: SettingsUseCaseImpl): SettingsUseCase
}