package ru.beesmart.carjoystick.settings.domain.models

class Settings(
    val lightOn: String,
    val lightOff: String,
    val maxSeekBarPosition: Int,
    val enableReverse: Boolean,
    val btAddress: String? = null
)