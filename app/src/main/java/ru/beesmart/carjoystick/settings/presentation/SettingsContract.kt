package ru.beesmart.carjoystick.settings.presentation

import ru.beesmart.carjoystick.base.presentation.PresentationContract
import ru.beesmart.carjoystick.joystick.presentation.JoystickContract

interface SettingsContract {

    interface View : PresentationContract.View{
        fun showAddressList(addressList: Map<String, String>)
        fun showStatus(status: String)
        fun connectBt()
    }

    interface Presenter : PresentationContract.Presenter<SettingsContract.View>{
        fun getBtDevices()
        fun setBtAddress(address: String)
    }
}