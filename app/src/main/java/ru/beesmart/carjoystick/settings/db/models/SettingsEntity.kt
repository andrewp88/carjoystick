package ru.beesmart.carjoystick.settings.db.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "settings")
data class SettingsEntity(
    val lightOn: String,
    val lightOff: String,
    val maxSeekBarPosition: Int,
    val enableReverse: Boolean,
    val btAddress: String?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}