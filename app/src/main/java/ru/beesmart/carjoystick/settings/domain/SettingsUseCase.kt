package ru.beesmart.carjoystick.settings.domain

import io.reactivex.Completable
import io.reactivex.Single

interface SettingsUseCase {
    fun getBtDevices(): Single<Map<String, String>>
    fun setAddress(address: String): Completable
}