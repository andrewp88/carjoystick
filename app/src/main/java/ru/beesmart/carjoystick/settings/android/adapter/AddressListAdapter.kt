package ru.beesmart.carjoystick.settings.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_address.view.*
import ru.beesmart.carjoystick.R
import ru.beesmart.carjoystick.base.android.adapter.BaseAdapter

class AddressListAdapter constructor(
    context: Context,
    private val clickListener: (mac: String) -> Unit
) : BaseAdapter<Pair<String, String>>(context) {

    fun addItems(addressList: Map<String, String>) {
        if (addressList.isNotEmpty()) {
            items.clear()

            items.addAll(0, addressList.toList())
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RowViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]

        holder.itemView.name.text = item.second
        holder.itemView.mac.text = item.first

        holder.itemView.setOnClickListener { clickListener.invoke(holder.itemView.mac.text.toString()) }
    }
}

class RowViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.row_address, parent, false))