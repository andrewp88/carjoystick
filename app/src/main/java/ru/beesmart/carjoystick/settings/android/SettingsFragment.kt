package ru.beesmart.carjoystick.settings.android


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_settings.*
import ru.beesmart.carjoystick.R
import ru.beesmart.carjoystick.main.android.OnNeedConnectionListener
import ru.beesmart.carjoystick.settings.android.adapter.AddressListAdapter
import ru.beesmart.carjoystick.settings.presentation.SettingsContract
import ru.beesmart.carjoystick.settings.presentation.SettingsPresenter
import javax.inject.Inject

class SettingsFragment : Fragment(), SettingsContract.View {

    @Inject
    lateinit var settingsPresenter: SettingsPresenter
    private lateinit var adapter: AddressListAdapter
    private lateinit var lm: LinearLayoutManager
    private lateinit var onNeedConnectionListener: OnNeedConnectionListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        onNeedConnectionListener = context as OnNeedConnectionListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lm = LinearLayoutManager(context)
        addressRV.layoutManager = lm

        adapter = AddressListAdapter(requireContext()) {
            settingsPresenter.setBtAddress(it)
        }
        addressRV.adapter = adapter

        settingsPresenter.getBtDevices()
    }

    override fun onResume() {
        settingsPresenter.bindView(this)
        super.onResume()
    }

    override fun onPause() {
        settingsPresenter.unbindView()
        super.onPause()
    }

    override fun showAddressList(addressList: Map<String, String>) {
        adapter.addItems(addressList)
    }

    override fun showStatus(status: String) {
        Toast.makeText(requireContext(), status, Toast.LENGTH_SHORT).show()
    }

    override fun connectBt() {
        onNeedConnectionListener.onNeedConnect()
    }
}