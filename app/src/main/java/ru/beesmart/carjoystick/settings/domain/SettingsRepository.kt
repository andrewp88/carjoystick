package ru.beesmart.carjoystick.settings.domain

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.settings.domain.models.Settings

interface SettingsRepository {
    fun getSettings(): Single<Settings>
    fun setAddress(address: String): Completable
}
