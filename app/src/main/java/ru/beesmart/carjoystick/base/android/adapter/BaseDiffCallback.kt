package ru.beesmart.carjoystick.base.android.adapter

import androidx.recyclerview.widget.DiffUtil

class BaseDiffCallback<T> : DiffUtil.Callback() {

    private lateinit var oldItems: List<T>
    private lateinit var newItems: List<T>

    override fun getOldListSize(): Int {
        return oldItems.size
    }

    override fun getNewListSize(): Int {
        return newItems.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return equals(oldItemPosition, newItemPosition)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return equals(oldItemPosition, newItemPosition)
    }

    fun setItems(oldItems: List<T>, newItems: List<T>) {
        this.oldItems = oldItems
        this.newItems = newItems
    }

    //    protected abstract boolean compareItem(int oldItemPosition, int newItemPosition);
    //    protected abstract boolean compareContent(int oldItemPosition, int newItemPosition);

    private fun equals(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (oldItemPosition > oldItems.size) return false
        if (newItemPosition > newItems.size) return false
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem == newItem
    }

}
