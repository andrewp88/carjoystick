package ru.beesmart.carjoystick.base.presentation

interface PresentationContract {
    interface View

    interface ViewWithProgress : View {
        fun showProgress()
        fun hideProgress()
    }

    interface Presenter<TView : View> {
        fun bindView(view: TView)
        fun unbindView()
        fun onDestroy()
    }
}
