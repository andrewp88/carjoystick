package ru.beesmart.carjoystick.base.android.adapter

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater

abstract class BaseAdapter<Item> constructor(
    context: Context,
    items: List<Item> = listOf(),
    private val diffCallback: BaseDiffCallback<Item> = BaseDiffCallback()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected val inflater: LayoutInflater = LayoutInflater.from(context)
    private var innerItems = items.toMutableList()

    override fun getItemCount(): Int {
        return innerItems.size
    }

    var items: MutableList<Item>
        get() = innerItems
        set(items) {
            diffCallback.setItems(this.innerItems, items)
            val diffResult = DiffUtil.calculateDiff(diffCallback)

            this.innerItems.clear()
            this.innerItems.addAll(items)
            diffResult.dispatchUpdatesTo(this)
        }

    fun deleteItem(item: Item) {
        val index = items.indexOf(item)
        items.remove(item)
        notifyItemRemoved(index)
    }
}