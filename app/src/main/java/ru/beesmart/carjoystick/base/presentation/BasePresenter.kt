package ru.beesmart.carjoystick.base.presentation

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ru.beesmart.carjoystick.extensions.WrapActions
import ru.beesmart.carjoystick.extensions.wrapWith

abstract class BasePresenter<T> protected constructor(){

    protected var view: T? = null
    protected var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var disposableSearch: Disposable? = null

    open fun bindView(view: T) {
        this.view = view
    }

    open fun unbindView() {
        this.view = null
    }

    open fun onDestroy() {
        compositeDisposable.dispose()
    }

    fun replaceSearchDisposable(disposable: Disposable) {
        if (disposableSearch != null) {
            compositeDisposable.delete(disposableSearch!!)
            disposableSearch!!.dispose()
        }
        disposableSearch = disposable
        compositeDisposable.add(disposableSearch!!)
    }

    protected fun Disposable.addToComposite() {
        compositeDisposable.add(this)
    }

    private val progressWrapActions = WrapActions(
        { (view as? PresentationContract.ViewWithProgress?)?.showProgress() },
        { (view as? PresentationContract.ViewWithProgress?)?.hideProgress() }
    )

    protected fun <T> Single<T>.wrapWithProgress() = wrapWith(progressWrapActions)

    protected fun <T> Maybe<T>.wrapWithProgress() = wrapWith(progressWrapActions)

/* commented for test coverage purpose (unused yet)
    protected fun <T> Completable.wrapWithProgress() = wrapWith(progressWrapActions)
*/
}
