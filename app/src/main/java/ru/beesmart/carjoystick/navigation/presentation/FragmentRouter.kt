package ru.beesmart.carjoystick.navigation.presentation

interface FragmentRouter {
    fun replaceToJoystickScreen()
    fun navigateToSettingsScreen()
    fun navigateBack()
}