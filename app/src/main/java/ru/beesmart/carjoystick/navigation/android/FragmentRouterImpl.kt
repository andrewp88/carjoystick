package ru.beesmart.carjoystick.navigation.android

import ru.beesmart.carjoystick.navigation.presentation.FragmentRouter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class FragmentRouterImpl @Inject constructor(
    private val router: Router
) : FragmentRouter {

    override fun replaceToJoystickScreen() {
        router.replaceScreen(Screens.JoystickScreen())
    }

    override fun navigateToSettingsScreen() {
        router.navigateTo(Screens.SettingsScreen())
    }

    override fun navigateBack() {
        router.exit()
    }
}
