package ru.beesmart.carjoystick.navigation.android

import androidx.fragment.app.Fragment
import ru.beesmart.carjoystick.joystick.android.JoystickFragment
import ru.beesmart.carjoystick.settings.android.SettingsFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    class JoystickScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return JoystickFragment()
        }
    }

    class SettingsScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return SettingsFragment()
        }
    }
}