package ru.beesmart.carjoystick.prefs.android

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.beesmart.carjoystick.prefs.data.SharedPrefs
import ru.beesmart.carjoystick.prefs.domain.PrefsRepository
import javax.inject.Inject

class PrefsRepositoryImpl @Inject
internal constructor(
    private val prefs: SharedPrefs
) : PrefsRepository {
    override fun saveBtAddress(address: String) {
        prefs.putString(BT_ADDRESS, address)
    }

    override fun getBtAddress(): Maybe<String?> {
        return Maybe.fromCallable{
            prefs.getString(BT_ADDRESS)
        }
            .subscribeOn(Schedulers.io())
    }

    override fun getFlashLightOnTag(): Single<String> {
        return Single.fromCallable {
            prefs.getString(FLASHLIGHT_ON, FLASHLIGHT_ON_TAG)
        }
            .subscribeOn(Schedulers.io())
    }

    override fun getFlashLightOffTag(): Single<String> {
        return Single.fromCallable {
            prefs.getString(FLASHLIGHT_OFF, FLASHLIGHT_OFF_TAG)
        }
            .subscribeOn(Schedulers.io())
    }

    companion object {
        private const val BT_ADDRESS = "bt_address"

        private const val FLASHLIGHT_ON = "flash_light_on"
        private const val FLASHLIGHT_OFF = "flash_light_off"
        private const val FLASHLIGHT_ON_TAG = "\$g;"
        private const val FLASHLIGHT_OFF_TAG = "\$f;"
    }
}
