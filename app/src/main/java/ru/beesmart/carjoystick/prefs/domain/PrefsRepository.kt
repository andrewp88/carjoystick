package ru.beesmart.carjoystick.prefs.domain

import io.reactivex.Maybe
import io.reactivex.Single

interface PrefsRepository {
    fun saveBtAddress(address: String)
    fun getBtAddress(): Maybe<String?>
    fun getFlashLightOnTag(): Single<String>
    fun getFlashLightOffTag(): Single<String>
}
