package ru.beesmart.carjoystick.prefs.di

import dagger.Binds
import dagger.Module
import ru.beesmart.carjoystick.prefs.android.PrefsRepositoryImpl
import ru.beesmart.carjoystick.prefs.data.SharedPrefs
import ru.beesmart.carjoystick.prefs.data.SharedPrefsImpl
import ru.beesmart.carjoystick.prefs.domain.PrefsRepository
import javax.inject.Singleton

@Module
abstract class PrefsModule {
    @Singleton
    @Binds
    internal abstract fun bindsRepository(impl: PrefsRepositoryImpl): PrefsRepository

    @Singleton
    @Binds
    internal abstract fun bindsSharedPrefs(impl: SharedPrefsImpl): SharedPrefs
}