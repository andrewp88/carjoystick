package ru.beesmart.carjoystick.prefs.data

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class SharedPrefsImpl @Inject
internal constructor(context: Context) : SharedPrefs {
    private val prefs: SharedPreferences = context.getSharedPreferences(NAME_PREFS, Context.MODE_PRIVATE)

    override fun getString(key: String, defaultValue: String): String {
        return prefs.getString(key, defaultValue)!!
    }

    override fun getString(key: String): String? {
        return prefs.getString(key, "")?.takeIf(String::isNotEmpty)
    }

    override fun putString(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return prefs.getBoolean(key, defaultValue)
    }

    override fun putBoolean(key: String, value: Boolean) {
        prefs.edit().putBoolean(key, value).apply()
    }

    override fun putStringSet(key: String, value: Set<String>) {
        prefs.edit().putStringSet(key, value).apply()
    }

    override fun getStringSet(key: String, defaultValue: Set<String>): Set<String> {
        return prefs.getStringSet(key, defaultValue)!!
    }

    override fun remove(key: String) {
        prefs.edit().remove(key).apply()
    }

    companion object {
        private const val NAME_PREFS = "arduJoyPrefs"
    }
}