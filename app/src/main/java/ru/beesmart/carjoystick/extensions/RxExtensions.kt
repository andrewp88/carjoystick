package ru.beesmart.carjoystick.extensions

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.disposables.Disposable

fun <T : Any> Single<T>.andThen(completable: Completable): Single<T> =
    flatMap { source ->
        completable
            .andThen(Single.just(source))
    }

fun <T : Any> Single<T>.andThen(completableFactory: (T) -> Completable) =
    flatMap { source ->
        completableFactory(source)
            .andThen(Single.just(source))
    }

/* TODO: uncomment when will need and will be covered in tests
fun <T : Any> Maybe<T>.andThen(completable: Completable): Maybe<T> =
    flatMap { source ->
        completable
            .andThen(Maybe.just(source))
    }
*/

fun <T : Any> T?.toMaybe() =
    Maybe.defer<T> {
        this?.let { Maybe.just(it) }
            ?: Maybe.empty()
    }

fun <T : Any> (() -> T?).toMaybe(): Maybe<T> = Maybe.defer {
    this()
        ?.let { Maybe.just(it) }
        ?: Maybe.empty()
}

fun (() -> Unit?).toCompletable(): Completable = Completable.defer {
    Completable.fromCallable { this() }
}

fun <T : Any> T.toSingle() =
    Single.defer<T> {
        Single.just(this)
    }

fun <T : Any> (() -> T).toSingle() =
    Single.defer<T> {
        Single.just(this())
    }

fun <T> Maybe<T>.wrapWith(wrapActions: WrapActions) = MaybeWrapper<T>(wrapActions).wrap(this)

fun <T> Single<T>.wrapWith(wrapActions: WrapActions) = SingleWrapper<T>(wrapActions).wrap(this)

/* commented for test coverage purpose (unused yet)
fun Completable.wrapWith(wrapActions: WrapActions) = CompletableWrapper(wrapActions).wrap(this)
*/

data class WrapActions(val onSubscribe: (Disposable) -> Unit, val onFinally: () -> Unit)

private abstract class Wrapper<T>(protected val wrapActions: WrapActions) {
    abstract fun wrap(source: T): T
}

private class SingleWrapper<T>(wrapActions: WrapActions) :
    Wrapper<Single<T>>(wrapActions) {
    override fun wrap(source: Single<T>) =
        source.doOnSubscribe(wrapActions.onSubscribe).doFinally(wrapActions.onFinally)
}

private class MaybeWrapper<T>(wrapActions: WrapActions) :
    Wrapper<Maybe<T>>(wrapActions) {
    override fun wrap(source: Maybe<T>) = source.doOnSubscribe(wrapActions.onSubscribe).doFinally(wrapActions.onFinally)
}

/*
private class CompletableWrapper(wrapActions: WrapActions) :
    Wrapper<Completable>(wrapActions) {
    override fun wrap(source: Completable) = source.doOnSubscribe(wrapActions.onSubscribe).doFinally(wrapActions.onFinally)
}
*/
