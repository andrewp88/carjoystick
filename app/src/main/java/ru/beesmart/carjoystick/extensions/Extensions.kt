package ru.beesmart.carjoystick.extensions

import ru.beesmart.carjoystick.settings.db.models.SettingsEntity
import ru.beesmart.carjoystick.settings.domain.models.Settings

fun Int.map(inMin: Int, inMax: Int, outMin: Int, outMax: Int): Int {
    return (this - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
}

fun SettingsEntity.mapToSettings(): Settings {
    return Settings(
        this.lightOn,
        this.lightOff,
        this.maxSeekBarPosition,
        this.enableReverse,
        this.btAddress
    )
}

fun Settings.mapToEntity(): SettingsEntity {
    return SettingsEntity(
        this.lightOn,
        this.lightOff,
        this.maxSeekBarPosition,
        this.enableReverse,
        this.btAddress
    )
}