package ru.beesmart.carjoystick.dagger

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ru.beesmart.carjoystick.CarJoystickApp
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(app: CarJoystickApp)
}
