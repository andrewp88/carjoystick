package ru.beesmart.carjoystick.dagger

import android.content.Context
import androidx.room.Room
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.beesmart.carjoystick.bluetooth.di.BluetoothModule
import ru.beesmart.carjoystick.joystick.android.JoystickFragment
import ru.beesmart.carjoystick.joystick.di.JoystickModule
import ru.beesmart.carjoystick.main.android.MainActivity
import ru.beesmart.carjoystick.main.di.MainModule
import ru.beesmart.carjoystick.navigation.android.FragmentRouterImpl
import ru.beesmart.carjoystick.navigation.di.NavigationModule
import ru.beesmart.carjoystick.navigation.presentation.FragmentRouter
import ru.beesmart.carjoystick.prefs.di.PrefsModule
import ru.beesmart.carjoystick.room.db.JoystickRoom
import ru.beesmart.carjoystick.settings.android.SettingsFragment
import ru.beesmart.carjoystick.settings.di.SettingsModule
import javax.inject.Singleton


@Module(includes = [
    AndroidSupportInjectionModule::class,
    AppProvidesModule::class,
    NavigationModule::class,
    PrefsModule::class,
    SettingsModule::class,
    BluetoothModule::class,
    MainModule::class,
    JoystickModule::class

])
abstract class AppModule {
    @ContributesAndroidInjector
    internal abstract fun mainInjector(): MainActivity

    @ContributesAndroidInjector(modules = [JoystickModule::class])
    internal abstract fun joystickInjector(): JoystickFragment

    @ContributesAndroidInjector
    internal abstract fun settingsInjector(): SettingsFragment

    @Singleton
    @Binds
    internal abstract fun bindsFragmentRouter(impl: FragmentRouterImpl): FragmentRouter
}

@Module
class AppProvidesModule {
    @Singleton
    @Provides
    fun providesDbRoom(context: Context): JoystickRoom {
        return Room
            .databaseBuilder(context, JoystickRoom::class.java, "JoystickBd")
            .build()
    }
}

