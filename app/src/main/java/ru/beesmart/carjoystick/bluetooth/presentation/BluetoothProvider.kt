package ru.beesmart.carjoystick.bluetooth.presentation

import io.reactivex.Completable
import io.reactivex.Single

interface BluetoothProvider {
    fun isAdapterEnabled(): Single<Boolean>
    fun getBoundedDevices(): Single<Map<String, String>>
    fun connectBt(address: String): Completable
    fun sendData(data: String): Completable
    fun getTemp(): Single<String>
}