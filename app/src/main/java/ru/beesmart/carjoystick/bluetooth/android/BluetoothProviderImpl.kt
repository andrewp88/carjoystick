package ru.beesmart.carjoystick.bluetooth.android

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.lang.NullPointerException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BluetoothProviderImpl @Inject constructor() : BluetoothProvider {

    private var btAdapter: BluetoothAdapter? = null
    private var btSocket: BluetoothSocket? = null
    private var outStream: OutputStream? = null
    private var inputStream: InputStream? = null

    override fun isAdapterEnabled(): Single<Boolean> {
        btAdapter = BluetoothAdapter.getDefaultAdapter()
        return Single.just(btAdapter?.isEnabled ?: false)
    }

    override fun getBoundedDevices(): Single<Map<String, String>> {
        return Single.just(getPairedDevices().mapValues { it.value.name })
    }

    override fun connectBt(address: String): Completable {
        val device = getPairedDevices()[address]
        return connectBTCompletable(device)
    }

    override fun sendData(data: String): Completable {
        return Completable.create {
            try {
                val msgBuffer = data.toByteArray()
                outStream?.write(msgBuffer)
                it.onComplete()
            } catch (e: IOException) {
                it.onError(e)
            }
        }
    }

    override fun getTemp(): Single<String> {
        return sendData("\$t;")
            .andThen(getBtTemp())
    }

    private fun getPairedDevices(): Map<String, BluetoothDevice> {
        return (btAdapter?.bondedDevices?.toList() ?: emptyList()).associateBy { it.address }
    }

    private fun connectBTCompletable(device: BluetoothDevice?): Completable {
        return device?.let {
            val uuid = device.uuids.first().uuid
             Completable.create {
                try {
                    btSocket = device.createRfcommSocketToServiceRecord(uuid)
                    btAdapter?.cancelDiscovery()
                    btSocket!!.connect()
                    it.onComplete()
                } catch (e: IOException) {
                    closeSocketCompletable()
                        .blockingAwait()
                    it.onError(e)
                }

            }
                .andThen(setStreamsCompletable())
                .andThen(
                    sendData("\$g;")
                        .delay(1000, TimeUnit.MILLISECONDS)
                        .andThen(sendData("\$f;"))
                )
        } ?: Completable.error(NullPointerException())
    }

    private fun closeSocketCompletable(): Completable {
        return Completable.create {
            try {
                btSocket!!.close()
                it.onComplete()
            } catch (e: IOException) {
                it.onError(e)
            }
        }
    }

    private fun setStreamsCompletable(): Completable {
        return Completable.create {
            try {
                outStream = btSocket!!.outputStream
                inputStream = btSocket!!.inputStream
                it.onComplete()
            } catch (e: IOException) {
                it.onError(e)
            }
        }
    }

    private fun getBtTemp() : Single<String> {
        return Single.create{emitter ->
            val reader = BufferedReader(inputStream!!.reader())
            var content: String
            reader.use {
                content = it.readText()
                emitter.onSuccess(content)
            }
        }
    }

    companion object {
        private val TAG = "bluetooth1"
    }
}

