package ru.beesmart.carjoystick.bluetooth.di

import dagger.Binds
import dagger.Module
import ru.beesmart.carjoystick.bluetooth.android.BluetoothProviderImpl
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import javax.inject.Singleton

@Module
abstract class BluetoothModule {

    @Singleton
    @Binds
    internal abstract fun bindBluetootchProvider(impl: BluetoothProviderImpl): BluetoothProvider
}