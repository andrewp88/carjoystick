package ru.beesmart.carjoystick.room.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.beesmart.carjoystick.settings.db.dao.SettingsDao
import ru.beesmart.carjoystick.settings.db.models.SettingsEntity

@Database(
    entities = [SettingsEntity::class],
    version = 1,
    exportSchema = false
)

abstract class JoystickRoom : RoomDatabase() {

    abstract val settings: SettingsDao
}