package ru.beesmart.carjoystick.joystick.di

import dagger.Binds
import dagger.Module
import ru.beesmart.carjoystick.bluetooth.android.BluetoothProviderImpl
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import ru.beesmart.carjoystick.joystick.data.JoystickRepositoryImpl
import ru.beesmart.carjoystick.joystick.domain.JoystickRepository
import ru.beesmart.carjoystick.joystick.domain.JoystickUseCase
import ru.beesmart.carjoystick.joystick.domain.JoystickUseCaseImpl
import ru.beesmart.carjoystick.joystick.presentation.JoystickContract
import ru.beesmart.carjoystick.joystick.presentation.JoystickPresenter

@Module
abstract class JoystickModule {

    @Binds
    internal abstract fun bindJoystickRepository(impl: JoystickRepositoryImpl) : JoystickRepository

    @Binds
    internal abstract fun bindJoystickUseCase(impl: JoystickUseCaseImpl) : JoystickUseCase

    @Binds
    internal abstract fun bindJoystickPresenter(impl: JoystickPresenter) : JoystickContract.Presenter
}