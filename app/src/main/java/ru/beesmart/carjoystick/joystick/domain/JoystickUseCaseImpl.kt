package ru.beesmart.carjoystick.joystick.domain

import io.reactivex.Completable
import io.reactivex.Single
import ru.beesmart.carjoystick.extensions.map
import ru.beesmart.carjoystick.settings.domain.SettingsRepository
import ru.beesmart.carjoystick.settings.domain.models.Settings
import javax.inject.Inject

class JoystickUseCaseImpl @Inject constructor(
    private val joystickRepository: JoystickRepository,
    private val settingsRepository: SettingsRepository
) : JoystickUseCase {

    private var reverse = false

    override fun setReverseMode(reverse: Boolean) {
        this.reverse = reverse
    }

    override fun sendSticksPos(speedPosition: Int, steeringPosition: Int): Completable {
        return settingsRepository.getSettings()
            .flatMapCompletable { settings ->
                convertAndSendData(settings, speedPosition, steeringPosition)
            }
    }

    override fun lightOn(): Completable {
        return settingsRepository.getSettings()
            .flatMapCompletable { settings ->
                joystickRepository.sendData(settings.lightOn)
            }
    }

    override fun lightOff(): Completable {
        return settingsRepository.getSettings()
            .flatMapCompletable { settings ->
                joystickRepository.sendData(settings.lightOff)
            }
    }

    override fun getTemp(): Single<String> {
        return joystickRepository.getTemp()
    }

    private fun convertAndSendData(
        settings: Settings,
        speedPosition: Int,
        steeringPosition: Int
    ): Completable {
        val maxSeekbarPos = settings.maxSeekBarPosition
        val speed = (if (reverse) -speedPosition else speedPosition).map(
           if (settings.enableReverse) -MAX_SEEKBAR_POSITION else MIN_SEEKBAR_POSITION,
            MAX_SEEKBAR_POSITION,
            -maxSeekbarPos,
            maxSeekbarPos
        )

        val angle = steeringPosition.map(
            MIN_SEEKBAR_POSITION,
            MAX_SEEKBAR_POSITION,
            -maxSeekbarPos,
            maxSeekbarPos
        )
        return joystickRepository.sendData("\$$angle $speed;")
    }

    companion object {
        const val MIN_SEEKBAR_POSITION = 0
        const val MAX_SEEKBAR_POSITION = 100
    }
}