package ru.beesmart.carjoystick.joystick.presentation

import ru.beesmart.carjoystick.base.presentation.PresentationContract

interface JoystickContract {

    interface View : PresentationContract.View{
        fun showSpeedPosition(speedPosition: Int)
        fun showSteeringPosition(steeringPosition: Int)
        fun showTemp(temp: String)
        fun onConnectNeeded()
    }

    interface Presenter : PresentationContract.Presenter<View>{
        fun onCruiseControlSwitched(switchOn: Boolean)
        fun onReversSwitched(switchOn: Boolean)
        fun onLightSwitched(switchOn: Boolean)
        fun setSticksPos(speedPosition:Int, steeringPosition: Int)
        fun onStopTrackingTouchMove()
        fun onStopTrackingTouchRotate()
        fun getTemp()
        fun showSettings()
    }
}