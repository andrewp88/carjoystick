package ru.beesmart.carjoystick.joystick.data

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.beesmart.carjoystick.bluetooth.presentation.BluetoothProvider
import ru.beesmart.carjoystick.joystick.domain.JoystickRepository
import javax.inject.Inject

class JoystickRepositoryImpl @Inject constructor(private val bluetoothProvider: BluetoothProvider) : JoystickRepository {
    override fun sendData(data: String): Completable {
        return bluetoothProvider.sendData(data)
            .subscribeOn(Schedulers.io())
    }

    override fun getTemp(): Single<String> {
        return bluetoothProvider.getTemp()
            .subscribeOn(Schedulers.io())
    }
}