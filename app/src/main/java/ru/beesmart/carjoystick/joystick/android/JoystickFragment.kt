package ru.beesmart.carjoystick.joystick.android


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_joystick.*

import ru.beesmart.carjoystick.R
import ru.beesmart.carjoystick.joystick.presentation.JoystickContract
import ru.beesmart.carjoystick.main.android.OnNeedConnectionListener
import javax.inject.Inject

class JoystickFragment : Fragment(), JoystickContract.View {

    @Inject lateinit var joystickPresenter: JoystickContract.Presenter
    private lateinit var onNeedConnectionListener: OnNeedConnectionListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        onNeedConnectionListener = context as OnNeedConnectionListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_joystick, container, false)
    }

    override fun onResume() {
        joystickPresenter.bindView(this)
        super.onResume()
    }

    override fun onPause() {
        joystickPresenter.unbindView()
        super.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        moveStick.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                joystickPresenter.setSticksPos(progress, rotationStick.progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                joystickPresenter.onStopTrackingTouchMove()
            }
        })

        rotationStick.isVerticalScrollBarEnabled = true

        rotationStick.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                joystickPresenter.setSticksPos(moveStick.progress, progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                joystickPresenter.onStopTrackingTouchRotate()
            }

        })

        cruiseControlSwitcher.setOnCheckedChangeListener { _, isChecked ->
            joystickPresenter.onCruiseControlSwitched(isChecked)
        }

        reverseSwitcher.setOnCheckedChangeListener { _, isChecked ->
            joystickPresenter.onReversSwitched(isChecked)
        }

        turnLightOnSwitcher.setOnCheckedChangeListener { _, isChecked ->
            joystickPresenter.onLightSwitched(isChecked)
        }

        indicator.setOnClickListener { joystickPresenter.getTemp() }
        settingsButton.setOnClickListener { joystickPresenter.showSettings() }
    }

    override fun showSpeedPosition(speedPosition: Int) {
        moveStick.progress = speedPosition
    }

    override fun showSteeringPosition(steeringPosition: Int) {
        rotationStick.progress = steeringPosition
    }

    override fun showTemp(temp: String) {
        tempDisplay.text = temp
    }

    override fun onConnectNeeded() {
        onNeedConnectionListener.onNeedConnect()
    }
}