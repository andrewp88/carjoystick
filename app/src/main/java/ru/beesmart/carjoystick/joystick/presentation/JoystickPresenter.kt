package ru.beesmart.carjoystick.joystick.presentation

import io.reactivex.android.schedulers.AndroidSchedulers
import ru.beesmart.carjoystick.base.presentation.BasePresenter
import ru.beesmart.carjoystick.joystick.domain.JoystickUseCase
import ru.beesmart.carjoystick.navigation.presentation.FragmentRouter
import javax.inject.Inject


class JoystickPresenter @Inject constructor(
    private val joystickUseCase: JoystickUseCase,
    private val fragmentRouter: FragmentRouter
) : BasePresenter<JoystickContract.View>(), JoystickContract.Presenter {

    private var cruiseControlMode = false

    override fun onCruiseControlSwitched(switchOn: Boolean) {
        cruiseControlMode = switchOn
        onStopTrackingTouchMove()
    }

    override fun onReversSwitched(switchOn: Boolean) {
        view?.showSpeedPosition(DEFAULT_SPEED)
        joystickUseCase.setReverseMode(switchOn)
    }

    override fun onLightSwitched(switchOn: Boolean) {
        if (switchOn) {
            joystickUseCase.lightOn()
        }  else {
            joystickUseCase.lightOff()
        }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {

                },
                {
                    view?.onConnectNeeded()
                }
            )
            .addToComposite()
    }

    override fun setSticksPos(speedPosition: Int, steeringPosition: Int) {
        joystickUseCase.sendSticksPos(speedPosition, steeringPosition)
            .subscribe(
                {

                },
                {
                    view?.onConnectNeeded()
                }
            )
            .addToComposite()
    }

    override fun getTemp() {
        joystickUseCase.getTemp()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view?.showTemp(it)
                },
                {
                    view?.onConnectNeeded()
                }
            )
            .addToComposite()
    }

    override fun onStopTrackingTouchMove() {
        if (!cruiseControlMode) {
            view?.showSpeedPosition(DEFAULT_SPEED)
        }
    }

    override fun onStopTrackingTouchRotate() {
        view?.showSteeringPosition(DEFAULT_STEERING_SEEKBAR_POSITION)
    }

    override fun showSettings() {
        fragmentRouter.navigateToSettingsScreen()
    }

    companion object {
        const val DEFAULT_SPEED = 0
        const val DEFAULT_STEERING_SEEKBAR_POSITION = 50
    }
}