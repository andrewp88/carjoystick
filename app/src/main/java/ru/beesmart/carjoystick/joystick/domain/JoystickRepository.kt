package ru.beesmart.carjoystick.joystick.domain

import io.reactivex.Completable
import io.reactivex.Single

interface JoystickRepository {
    fun sendData(data: String): Completable
    fun getTemp(): Single<String>
}