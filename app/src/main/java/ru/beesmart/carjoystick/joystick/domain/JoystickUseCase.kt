package ru.beesmart.carjoystick.joystick.domain

import io.reactivex.Completable
import io.reactivex.Single

interface JoystickUseCase {
    fun sendSticksPos(speedPosition: Int, steeringPosition: Int): Completable
    fun setReverseMode(reverse: Boolean)
    fun lightOn(): Completable
    fun lightOff(): Completable
    fun getTemp(): Single<String>
}